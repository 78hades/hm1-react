

export default function Button({ type, classNames, onClick, children, style }) {

    return (
       <button type={type} className={classNames} onClick={onClick} style={style}>
           {children}
       </button>
    );
}