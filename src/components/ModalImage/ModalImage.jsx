import image from "./image.png";

export default function ModalImage() {
  return <img className="modal__image" src={image} alt="" />;
}
