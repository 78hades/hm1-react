export default function ModalFooter({firstText, secondaryText, firstClick, secondaryClick}){
    return(
    <div className="modal__footer">
        {firstText && firstClick && (
            <button onClick={firstClick} className='modal__first-btn'>{firstText}</button>
        )}
          {secondaryText && secondaryClick && (
            <button onClick={secondaryClick} className='modal__second-btn'>{secondaryText}</button>
        )}
    </div>
    )
}