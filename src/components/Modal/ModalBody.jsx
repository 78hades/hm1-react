export default function ModalBody({ children }) {
  return <p className="modal__body">{children}</p>;
}
