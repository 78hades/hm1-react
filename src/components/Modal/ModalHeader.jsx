export default function ModalHeader({ children }) {
  return <h1 className="modal__title">{children}</h1>;
}
