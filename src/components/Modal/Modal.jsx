import ModalWrapper from "./ModalWrapper";
import ModalHeader from "./ModalHeader";
import ModalImage from "../ModalImage/ModalImage";
import ModalClose from "./ModalClose";
import ModalBody from "./ModalBody";
import ModalFooter from "./ModalFooter";
export default function Modal({
  stateOne,
  stateTwo,
  onCloseFirst,
  onCloseSecond,
}) {
  return (
    <>
      {stateOne && (
        <ModalWrapper onClick={onCloseFirst}>
          <div className="modal__content" onClick={(e) => e.stopPropagation()}>
            <ModalClose onClick={onCloseFirst}></ModalClose>
            <ModalImage></ModalImage>
            <ModalHeader>Product Delete!</ModalHeader>
            <ModalBody>
              By clicking the “Yes, Delete” button, PRODUCT NAME will be
              deleted.
            </ModalBody>

            <ModalFooter
              firstText="NO, CANCEL"
              firstClick={() => console.log("clicked the first button")}
              secondaryText="YES, DELETE"
              secondaryClick={() => console.log("clicked the second button")}
            ></ModalFooter>
          </div>
        </ModalWrapper>
      )}
      {stateTwo && (
        <div>
          <ModalWrapper onClick={onCloseSecond}>
            <div
              className="modal__content"
              onClick={(e) => e.stopPropagation()}
            >
              <ModalClose onClick={onCloseSecond}></ModalClose>
              <ModalHeader>Add Product "NAME"</ModalHeader>
              <ModalBody>Description for you product</ModalBody>
              <ModalFooter
                firstText="ADD TO FAVORITE"
                firstClick={() => console.log("clicked the first button")}
              ></ModalFooter>
            </div>
          </ModalWrapper>
        </div>
      )}
    </>
  );
}
