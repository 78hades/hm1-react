export default function ModalClose({ onClick }) {
  return <div className="modal__close" onClick={onClick}></div>;
}
