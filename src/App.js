
import './App.scss'
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";
import { useState } from "react";

function App() {
  const [stateModalFirst, setStateModalFirst] = useState(false);
  const [stateModalSecond, setStateModalSecond] = useState(false);

  return (
 <>
      <Button
        type="button"
        classNames="modal__first-btn"
        onClick={() => setStateModalFirst(true)}
      >
        Open first modal
      </Button>
      <Button
        type="button"
        classNames="modal__second-btn"
        onClick={() => setStateModalSecond(true)}
      >
        Open second modal
      </Button>
      <Modal
        stateOne={stateModalFirst}
        onCloseFirst={() => setStateModalFirst(false)}
        stateTwo={stateModalSecond}
        onCloseSecond={() => setStateModalSecond(false)}
      >
        {" "}
      </Modal>
</>
  );
}

export default App;
